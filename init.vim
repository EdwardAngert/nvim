" 1: mac: ~/.config/nvim/init.vim
" 2: install vim-plug https://github.com/junegunn/vim-plug  then :PlugInstall
set number " line numbers
syntax enable
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab " tabs are spaces
set cursorline " highlight current line
set wildmenu " show autocomplete menus
set hidden " keep undo history after buffer reload
set mouse=a
set guifont=Karambasi\ Book " obvs, install font first or comment this out
call plug#begin('~/.local/share/nvim/site/autoload')
  Plug 'godlygeek/tabular'
  Plug 'raphamorim/lucario'
  Plug 'tpope/vim-fugitive'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'plasticboy/vim-markdown'
  Plug 'tanvirtin/monokai.nvim'
  " Plug 'dracula/vim', { 'as': 'dracula'}
  Plug 'scrooloose/nerdtree'
  Plug 'severin-lemaignan/vim-minimap'
  Plug 'Yggdroot/indentLine'
call plug#end()
colorscheme monokai
" was lucario and torte was a good colorscheme too; tried dracula and like some colors, but
" it's a little dark 

" Airline
set laststatus=2
let g:airline#extensions#tabline#enabled=1
let g:airline_theme='bubblegum' " liked molokai and dracula
let g:airline_powerline_fonts=1
let g:airline#extensions#branch#enabled=1

" Vim markdown
let g:vim_markdown_frontmatter = 1 " yaml syntax highlighting
let g:vim_markdown_folding_disabled = 1 " vim-markdown plugin defaults to folding. this stops that behavior
let g:vim_markdown_conceal = 0 " vim-markdown plugin hides some markdown that I'd rather see. this stops that behavior
let g:vim_markdown_emphasis_multiline = 0 " keep vim-markdown from emphasising multiline?

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif " if nerdtree is the only thing open, allow easier exit

" the following are key bindings - place comment describing binding first

" toggle nerdtree with ctrl+n
" map <C-n> :NERDTreeToggle<CR>

" Indent Guides - options for indentLine plug
let g:indentLine_enabled=1
let g:indentLine_color_term=235
let g:indentLine_char='┆'

" Browse airline tabs
:nnoremap <C-p> :bnext<CR>
:nnoremap <C-o> :bprevious<CR>

" Map Control S for save
noremap <silent> <C-S> :update<CR>
vnoremap <silent> <C-S> <C-C>:update<CR>
inoremap <silent> <C-S>  <C-O>:update<CR>

" Comment block
vnoremap <silent> <C-k> :Commentary<cr>

" Close current buffer
noremap <silent> <C-q> :Bclose!<CR>

" Toggle Nerdtree
noremap <silent> <C-f> ::NERDTreeToggle<CR>

" Select all
map <C-a> <esc>ggVG<CR>

" Start Minimap
" autocmd VimEnter * Minimap

" Delete buffer while keeping window layout (don't close buffer's windows).
" Version 2008-11-18 from http://vim.wikia.com/wiki/VimTip165
if v:version < 700 || exists('loaded_bclose') || &cp
  finish
endif
let loaded_bclose = 1
if !exists('bclose_multiple')
  let bclose_multiple = 1
endif

" Display an error message.
function! s:Warn(msg)
  echohl ErrorMsg
  echomsg a:msg
  echohl NONE
endfunction

" Close buffer properly with NERDtree
" http://stackoverflow.com/questions/1864394/vim-and-nerd-tree-closing-a-buffer-properly
"
" Command ':Bclose' executes ':bd' to delete buffer in current window.
" The window will show the alternate buffer (Ctrl-^) if it exists,
" or the previous buffer (:bp), or a blank buffer if no previous.
" Command ':Bclose!' is the same, but executes ':bd!' (discard changes).
" An optional argument can specify which buffer to close (name or number).
function! s:Bclose(bang, buffer)
  if empty(a:buffer)
    let btarget = bufnr('%')
  elseif a:buffer =~ '^\d\+$'
    let btarget = bufnr(str2nr(a:buffer))
  else
    let btarget = bufnr(a:buffer)
  endif
  if btarget < 0
    call s:Warn('No matching buffer for '.a:buffer)
    return
  endif
  if empty(a:bang) && getbufvar(btarget, '&modified')
    call s:Warn('No write since last change for buffer '.btarget.' (use :Bclose!)')
    return
  endif
  " Numbers of windows that view target buffer which we will delete.
  let wnums = filter(range(1, winnr('$')), 'winbufnr(v:val) == btarget')
  if !g:bclose_multiple && len(wnums) > 1
    call s:Warn('Buffer is in multiple windows (use ":let bclose_multiple=1")')
    return
  endif
  let wcurrent = winnr()
  for w in wnums
      execute w.'wincmd w'
    let prevbuf = bufnr('#')
    if prevbuf > 0 && buflisted(prevbuf) && prevbuf != w
      buffer #
    else
      bprevious
    endif
    if btarget == bufnr('%')
      " Numbers of listed buffers which are not the target to be deleted.
      let blisted = filter(range(1, bufnr('$')), 'buflisted(v:val) && v:val !=
      btarget')
      " Listed, not target, and not displayed.
      let bhidden = filter(copy(blisted), 'bufwinnr(v:val) < 0')
      " Take the first buffer, if any (could be more intelligent).
      let bjump = (bhidden + blisted + [-1])[0]
      if bjump > 0
        execute 'buffer '.bjump
      else
        execute 'enew'.a:bang
      endif
    endif
  endfor
  execute 'bdelete'.a:bang.' '.btarget
  execute wcurrent.'wincmd w'
endfunction
command! -bang -complete=buffer -nargs=? Bclose call <SID>Bclose('<bang>','<args>')
nnoremap <silent> <Leader>bd :Bclose<CR>
nnoremap <silent> <Leader>bD :Bclose!<CR>

" Change cursor appearance depending on the current mode
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"
